//By Garreth Byrne
#include "DestructibleBlock.h"

DestructibleBlock::DestructibleBlock():Entity(){
	spriteData.x = garreth::X;              // location on screen
	spriteData.y = garreth::Y;
	setCurrentFrame(startFrame);
	collisionType = entityNS::BOX;
	
	active = true;
	visible = true;
	ID = 0;
}

bool DestructibleBlock::initialize(Game *gamePtr, int width, int height, int ncols,
	TextureManager *textureM)
{
	//idle.initialize()
	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void DestructibleBlock::draw()
{
	Image::draw();              // draw Player

}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void DestructibleBlock::update(float frameTime)
{
/*	if (active){
		visible = true;
	}
	else{
		visible = false;
	}*/
	Entity::update(frameTime);

}

//=========================================
//networking
//=========================================
DblockStc DestructibleBlock::getNetData(){
	DblockStc data;
	data.X;
	data.Y;
	data.active = active;
	return data;
}

void DestructibleBlock::setNetData(DblockStc ds){
	
	if (active)
		setVisible(true);
	else
		setVisible(false);
	setX(ds.X);
	setY(ds.Y);
	
}