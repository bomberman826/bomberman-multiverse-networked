#ifndef _DESTRUCTIBLEBLOCK_H
#define _DESTRUCTIBLEBLOCK_H
#define WIN32_LEAN_AND_MEAN
#include "entity.h"
#include "constants.h"

namespace garreth
{
	const int   WIDTH = 32;                // image width
	const int   HEIGHT = 32;               // image height
	const int   X = 0; // location on screen
	const int   Y = 0;
	const int   TEXTURE_COLS = 1;       // texture has 2 columns
	const int   START_FRAME = 1;        // starts at frame 1
	const int   END_FRAME = 1;          // no animation

}
struct DblockStc
{
	float X, Y;
	//UCHAR flags;
	bool active;
	//bit0 active
};
class DestructibleBlock :public Entity
{
private :
	int ID;
public:
	DestructibleBlock();
	virtual void draw();
	virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
		TextureManager *textureM);
	void update(float frameTime);
	int getID(){ return ID; }
	void setID(int p){ ID = p; }
	//Networking 
	DblockStc getNetData();
	void setNetData(DblockStc ds);
};

#endif