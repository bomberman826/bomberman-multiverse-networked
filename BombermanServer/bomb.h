#ifndef BOMB_H               // Prevent multiple definitions if this 
#define BOMB_H              // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"
#include "player.h"
#include "audio.h"
using namespace Conor;

namespace ConorBomb
{
	const int WIDTH = 32;                   // image width
	const int HEIGHT = 32;                  // image height
	const int X = 0;   // location on screen
	const int Y = 0;
	const float ROTATION_RATE = (float)PI / 4; // radians per second
	const float SPEED = 10;                // 100 pixels per second
	const float MASS = 300.0f;              // mass
	const int   TEXTURE_COLS = 3;           // texture has 8 columns

	//Bomb
	const int   BOMB_START_FRAME = 0;    // engine start frame
	const int   BOMB_END_FRAME = 1;      // engine end frame
	const float BOMB_ANIMATION_DELAY = 1.5f;  // time between frames
}
//BombStc
struct BombStc
{
	float X, Y;
	bool active;
	float fuse;
	bool isExploded;
};
// inherits from Entity class
class Bomb : public Entity
{
private:
	Image   bomb;
	float fuse;
	bool isExploded;
	bool canBePlaced;
public:
	// constructor
	Bomb();
	
	// inherited member functions
	virtual void draw();
	virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
		TextureManager *textureM);
	void plant(Entity *player,float p_fuse);              
	void update(float frameTime);
	bool getIsexploded(){ return isExploded; }
	void setExploded(bool p){ isExploded = p; }
	float getFuse(){ return fuse; }
	void setFuse(float p){ fuse = p; }
	bool getPlaced(){ return canBePlaced; }
	void setPlaced(bool p){ canBePlaced = p; }
	//networking
	BombStc getNetData();
	void setNetData(BombStc bs);
};
#endif
