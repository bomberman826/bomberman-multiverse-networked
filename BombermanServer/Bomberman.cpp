// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// createThisClass.cpp v1.1
// This class is the core of the game

#include "Bomberman.h"
using namespace Conor;
using namespace garreth;

//=============================================================================
// Constructor
//=============================================================================
Bomberman::Bomberman()
{

	mapX = garreth::MAP_POSITIONX;
	mapY = garreth::MAP_POSITIONY;
	isPlaying = false;
	countDownOn = false;
    messageY = 0;
	countDownTimer = garreth::ROUND_COUNT_DOWN;
	roundOver = false;
	startTimerRun = false;
	startTimer = 0;
	state = START_OPTION;
	pointer.setX(1100);
	pointer.setY(400);

	//NETWORK
	port = netNS::DEFAULT_PORT;
	netTime = 0;
}

//=============================================================================
// Destructor
//=============================================================================
Bomberman::~Bomberman()
{
	menuOn = true;
	controlsOn = true;
    releaseAll();           // call onLostDevice() for every graphics item
  //  safeDelete(dxFont);
	//safeDelete(fontScore);

}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Bomberman::initialize(HWND hwnd)
{
	Game::initialize(hwnd); // throws GameError

	// menu texture
	if (!menuTextures.initialize(graphics, MENU_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu texture"));
	// menu texture
	if (!controlTextures.initialize(graphics, CONTROLS_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu texture"));
	// MENU SPRITE
	if (!menuSpriteTexture.initialize(graphics, START))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!menuSpriteTexture2.initialize(graphics, CONTROLS))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!menuSpriteTexture3.initialize(graphics, QUIT))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!menuSpriteTexture4.initialize(graphics, BACK))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!pointerTextures.initialize(graphics, POINTER_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!player1Texture.initialize(graphics, BOMBERMAN_PLAYER1))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!player2Texture.initialize(graphics, BOMBERMAN_PLAYER2))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	// tile textures
	if (!tileTextures.initialize(graphics, TILES))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing tile textures"));

	// main game textures
	if (!playerTextures.initialize(graphics, PLAYERS_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));
	if (!player2Textures.initialize(graphics, PLAYER2_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));
	if (!player3Textures.initialize(graphics, PLAYER3_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));
	if (!player4Textures.initialize(graphics, PLAYER4_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));
	if (!player5Textures.initialize(graphics, PLAYER5_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));
	if (!bombTexture.initialize(graphics, BOMB_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!dBlockTexture.initialize(graphics, DESBLOCK_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!powerUpTexture.initialize(graphics, POWERUPBLOCK_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!explosionTexture.initialize(graphics, EXPLOSION_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!explosionTexture2.initialize(graphics, MID_EXPLOSION_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!explosionTexture3.initialize(graphics, LARGE_EXPLOSION_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));
	// tile image
	if (!tile.initialize(graphics, 0, 0, 0, &tileTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing tile"));

	// menu image
	if (!menu.initialize(graphics, 0, 0, 0, &menuTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu"));
	// menu image
	if (!controls.initialize(graphics, 0, 0, 0, &controlTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu"));
	// menu image
	if (!pointer.initialize(graphics, 0, 0, 0, &pointerTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu"));
	if (!Player1Image.initialize(graphics, 0, 0, 0, &player1Texture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player 1 image"));
	if (!Player2Image.initialize(graphics, 0, 0, 0, &player2Texture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player 2 image"));
	Player1Image.setY(200);
	Player2Image.setY(200);
	Player2Image.setX(1100);

	//fonts
	
	
	fontScore.initialize(graphics, 48, true, false, "Arial");
	fontScore.setFontColor(graphicsNS::BLUE);
	dxFont.initialize(graphics, 48, true, false, "Arial");
	dxFont.setFontColor(graphicsNS::GREEN);
	roundTimeFont.initialize(graphics, 100, true, false, "Arial");

	roundTimeFont.setFontColor(graphicsNS::RED);
	fontScoreP3.initialize(graphics, 48, true, false, "Arial");
	fontScoreP3.setFontColor(graphicsNS::RED);
	fontScoreP4.initialize(graphics, 48, true, false, "Arial");
	fontScoreP4.setFontColor(graphicsNS::PURPLE);
	fontScoreP5.initialize(graphics, 48, true, false, "Arial");
	fontScoreP5.setFontColor(graphicsNS::YELLOW);

	mapIn.open(MAP);
	if (!mapIn)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error loading map data"));
	for (int row = 0; row < MAP_HEIGHT; row++)
	{
		for (int col = 0; col < MAP_WIDTH; col++)
			mapIn >> tileMap[row][col];

	}

	//Menu
	menuSprite.initialize(graphics, &menuSpriteTexture, input, hwnd, 1200, 400, 1.5f, true);
	menuSprite2.initialize(graphics, &menuSpriteTexture2, input, hwnd, 1200, 500, 1.5f, true);
	menuSprite3.initialize(graphics, &menuSpriteTexture3, input, hwnd, 1200, 600, 1.5f, true);
	menuSprite4.initialize(graphics, &menuSpriteTexture4, input, hwnd, 750, 400, 1.5f, true);


	//Player
	if (!players[0].initialize(this, Conor::WIDTH, Conor::HEIGHT, Conor::TEXTURE_COLS, &playerTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player1"));
	players[0].setFrames(Conor::SPRITE_START_FRAME, Conor::SPRITE_END_FRAME);
	players[0].setCurrentFrame(Conor::IDLE_FRAME);
	players[0].setVelocity(VECTOR2(Conor::SPEED, -Conor::SPEED)); // VECTOR2(X, Y)
	players[0].setKeys(PLAYER1_RIGHT_KEY, PLAYER1_LEFT_KEY, PLAYER1_UP_KEY, PLAYER1_DOWN_KEY, PLAYER1_DROP_KEY);
	players[0].setGamepad(0);
	players[0].setEdge(COLLISION_BOX);
	players[0].setID(1);

	//player1.setCollisionRadius(Conor::RADUIS);
	//Player2
	if (!players[1].initialize(this, Conor::WIDTH, Conor::HEIGHT, Conor::TEXTURE_COLS, &player2Textures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player2"));
	players[1].setFrames(Conor::SPRITE_START_FRAME, Conor::SPRITE_END_FRAME);
	players[1].setCurrentFrame(Conor::IDLE_FRAME);
	players[1].setVelocity(VECTOR2(Conor::SPEED, -Conor::SPEED)); // VECTOR2(X, Y)
	players[1].setKeys(PLAYER2_RIGHT_KEY, PLAYER2_LEFT_KEY, PLAYER2_UP_KEY, PLAYER2_DOWN_KEY, PLAYER2_DROP_KEY);
	players[1].setGamepad(1);
	players[1].setEdge(COLLISION_BOX);
	players[1].setID(2);

	if (!players[2].initialize(this, Conor::WIDTH, Conor::HEIGHT, Conor::TEXTURE_COLS, &player3Textures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player2"));
	players[2].setFrames(Conor::SPRITE_START_FRAME, Conor::SPRITE_END_FRAME);
	players[2].setCurrentFrame(Conor::IDLE_FRAME);
	players[2].setEdge(COLLISION_BOX);
	players[2].setID(3);

	if (!players[3].initialize(this, Conor::WIDTH, Conor::HEIGHT, Conor::TEXTURE_COLS, &player4Textures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player2"));
	players[3].setFrames(Conor::SPRITE_START_FRAME, Conor::SPRITE_END_FRAME);
	players[3].setCurrentFrame(Conor::IDLE_FRAME);
	players[3].setEdge(COLLISION_BOX);
	players[3].setID(4);

	if (!players[4].initialize(this, Conor::WIDTH, Conor::HEIGHT, Conor::TEXTURE_COLS, &player5Textures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player2"));
	players[4].setFrames(Conor::SPRITE_START_FRAME, Conor::SPRITE_END_FRAME);
	players[4].setCurrentFrame(Conor::IDLE_FRAME);
	players[4].setEdge(COLLISION_BOX);
	players[4].setID(5);


	tileEntity.initialize(this, TILE_SIZE, TILE_SIZE, 1, &tileTextures);
	tileEntity.setEdge(garreth::TILE_EDGE);  // for tile collision
	tileEntity.setCollisionType(entityNS::BOX);

	//loads the level of blocks in the game
	loadLevel();


	for (int i = 0; i < garreth::NUMBER_OF_POWERUPS; i++){
		if (!powerUps[i].initialize(this, 32, 32, 10, &powerUpTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}
		int a = 0;
		int count = 0;
		int end = 32;
		powerUps[i].setEdge(garreth::POWER_UP_EDGE);
		powerUps[a].setFrames(count, end);
		powerUps[i].setID(i + 1);
		powerUps[i].setActive(false);
		powerUps[i].setVisible(false);
		count += 32;
		end += 32;
		a++;
		if (a > 10){
			a = 0;
		}
	}

	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		if (!bombs[i].initialize(this, ConorBomb::WIDTH, ConorBomb::HEIGHT, ConorBomb::TEXTURE_COLS, &bombTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing bomb"));
		bombs[i].setEdge(garreth::TILE_EDGE);
		bombs[i].setFrames(ConorBomb::BOMB_START_FRAME, ConorBomb::BOMB_END_FRAME);
	}
	
		
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		if (!explosionSmall[i].initialize(this, 0, 0, 0, &explosionTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing explosion"));
		explosionSmall[i].setCollisionRadius(garrethExplosion::COLLISION_RADIUS);
	}
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		if (!explosionMid[i].initialize(this, garrethExplosion::MID_WIDTH, garrethExplosion::MID_HEIGHT, 0, &explosionTexture2))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing explosion"));
		explosionMid[i].setCollisionRadius(garrethExplosion::MID_COLLISION_RADIUS);
	}
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		if (!explosionLarge[i].initialize(this, garrethExplosion::LARGE_WIDTH, garrethExplosion::LARGE_HEIGHT, 0, &explosionTexture3))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing explosion"));
		explosionLarge[i].setCollisionRadius(garrethExplosion::LARGE_COLLISION_RADIUS);
	}
	
	


		toClientData.gameState = 0;
		toClientData.sounds = 0;
		initializeServer(port);     // initialize game server
		roundOver = true;
    return;
}

//=============================================================================
// Update all game items
//=============================================================================
void Bomberman::update()
{
	int visibilebPlayerCount = 0;
	playerCount = 0;
	console->show();
	if (startTimerRun){
		startTimer += frameTime;
		if (startTimer >= 5){
			startTimer = 0;
			startTimerRun = false;
			roundStart();
		}
	}

	if (countDownOn){
		countDownTimer -= frameTime;
		if (countDownTimer <= 0){
			countDownOn = false;
			toClientData.gameState &= (0xFF ^ ROUND_START_BIT);
		}
	}
	else{
		for (int i = 0; i < garreth::MAX_PLAYERS; i++){
			if (players[i].getConnected()){
				playerCount++;
			}
			if (players[i].getVisible()){
				visibilebPlayerCount++;
			}
			if (players[i].getActive()){
				if (players[i].getButtons() & FORWARD_BIT){
					players[i].setPlayerState(WALKING);
					players[i].setDirection(NORTH);
					players[i].setFrames(Conor::UP_START_FRAME, Conor::UP_END_FRAME);
					toClientData.sounds ^= WALK_BIT;
				}
				else if (players[i].getButtons() & BACKWARDS_BIT){
					players[i].setPlayerState(WALKING);
					players[i].setDirection(SOUTH);			
					toClientData.sounds ^= WALK_BIT;
				}
				else if (players[i].getButtons() & LEFT_BIT){
					players[i].setPlayerState(WALKING);
					players[i].setDirection(EAST);
					toClientData.sounds ^= WALK_BIT;
				}
				else if (players[i].getButtons() & RIGHT_BIT){
					players[i].setPlayerState(WALKING);
					players[i].setDirection(WEST);
					toClientData.sounds ^= WALK_BIT;
				}
				else if (players[i].getButtons() & BOMBPLACE_BIT){
					handleBombs(&players[i]);
				}
				else{
					players[i].setPlayerState(STANDING);
				}
			}
			players[i].update(frameTime);
			bombs[i].update(frameTime);

		}
	}

	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		explosionSmall[i].update(frameTime);
	}
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		explosionMid[i].update(frameTime);
	}
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		explosionLarge[i].update(frameTime);
	}
	//check to see what explosions trigger
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		if (players[i].getBombPower() == 1){
			if (bombs[i].getFuse() < 0 && bombs[i].getFuse() > -0.2f){
				//explode.explode(&bombs[0]);
				explosionSmall[i].explode(&bombs[i]);
			}
		}
		else if (players[i].getBombPower() == 2){
			if (bombs[i].getFuse() < 0 && bombs[i].getFuse() > -0.2f){
				explosionMid[i].explode(&bombs[i]);
			}

		}
		else if ((players[i].getBombPower() == 3)){
			if (bombs[i].getFuse() < 0 && bombs[i].getFuse() > -0.2f){
				explosionLarge[i].explode(&bombs[i]);
			}
		}
	}
	for (int i = 0; i < garreth::NUMBER_OF_POWERUPS; i++){
		powerUps[i].update(frameTime);
	}
	for (int i = 0; i < garreth::NUMBER_OF_DBLOCKS; i++){
		dBlocks[i].update(frameTime);
	}
	
	if (playerCount >= 1 && (visibilebPlayerCount <= 1 || roundOver)){
		startTimerRun = true;
	}

}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void Bomberman::ai()
{}

//=============================================================================
// Handle collisions
//=============================================================================
void Bomberman::collisions()
{
	VECTOR2 collisionVector;
	UCHAR sounds = toClientData.sounds;
	pickUp = rand() % 8 - 1;
	for (int i = 0; i < garreth::MAX_PLAYERS; i++)
	{
		for (int j = i + 1; j < garreth::MAX_PLAYERS; j++){
			if (players[i].collidesWith(players[j],collisionVector)){
				players[i].toOldPosition();
				players[j].toOldPosition();
			}
		}
		for (int j = 0; j < garreth::NUMBER_OF_DBLOCKS; j++){
			if (players[i].collidesWith(dBlocks[j], collisionVector)){
				players[i].toOldPosition();
			}
		}
		for (int j = 0; j < garreth::MAX_PLAYERS; j++){
			if (players[i].collidesWith(bombs[j], collisionVector)){
				players[i].toOldPosition();
			}
		}
	for (int j = 0; j < garreth::NUMBER_OF_POWERUPS; j++){
			if (players[i].collidesWith(powerUps[j], collisionVector)){
				players[i].addPowerUP(powerUps[j]);
				powerUps[j].setActive(false);
				powerUps[j].setVisible(false);
				if (sounds & PICKUP_BIT){
					toClientData.sounds &= (0xFF ^ PICKUP_BIT);
				}
				else{
					toClientData.sounds ^= PICKUP_BIT;
				}

			}
		}
	}
	for (int i = 0; i < garreth::NUMBER_OF_DBLOCKS; i++){
		if (dBlocks[i].getActive()){
			for (int j = 0; j < garreth::MAX_PLAYERS; j++){
				if (dBlocks[i].collidesWith(explosionSmall[j], collisionVector) || dBlocks[i].collidesWith(explosionMid[j], collisionVector) || dBlocks[i].collidesWith(explosionLarge[j], collisionVector)){
					dBlocks[i].setActive(false);
					dBlocks[i].setVisible(true);
					
					//spawn a random power up if its not active already
					if (!powerUps[pickUp].getActive()){
						powerUps[pickUp].setX(dBlocks[i].getX());
						powerUps[pickUp].setY(dBlocks[i].getY());
						powerUps[pickUp].setActive(true);
						powerUps[pickUp].setVisible(true);
					}
				}
			}
		}
	}
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		for (int j = 0; j < garreth::MAX_PLAYERS; j++){
			if (i == j){
				if (players[i].collidesWith(explosionSmall[j], collisionVector) || players[i].collidesWith(explosionMid[j], collisionVector) || players[i].collidesWith(explosionLarge[j], collisionVector)){

					players[i].setScore(players[i].getScore() - 10);
					players[i].setActive(false);
					if (sounds & DIED_BIT){
						toClientData.sounds &= (0xFF ^ DIED_BIT);
					}
					else{
						toClientData.sounds |= DIED_BIT;
					}
				}
			}
			else if (i != j){
				if (players[i].collidesWith(explosionSmall[j], collisionVector) || players[i].collidesWith(explosionMid[j], collisionVector) || players[i].collidesWith(explosionLarge[j], collisionVector)){
					players[j].setScore(players[j].getScore() + 10);
					players[i].setActive(false);
					if (sounds & DIED_BIT){
						toClientData.sounds &= (0xFF ^ DIED_BIT);
					}
					else{
						toClientData.sounds |= DIED_BIT;
					}
				}
			}

		}
	}
		
	
			
	handleTileCollision(collisionVector);

	
}
//=============================================================================
// Name: handleTileCollision
// Description: handles collision for the tile map (from Tile Collision )
// Parameters: Vector2 collsionVector
// Returns: none
// Garreth
//=============================================================================
void Bomberman::handleTileCollision(VECTOR2 collisionVector){
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		UINT player1Row = static_cast<UINT>((players[i].getY() - mapY) / TILE_SIZE);
		UINT player1Col = static_cast<UINT>((players[i].getX() - mapX) / TILE_SIZE);

		if (player1Col > 0 && player1Row < MAP_HEIGHT && tileMap[player1Row][player1Col - 1] <= garreth::MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>((player1Col - 1)*TILE_SIZE + mapX));    // position tileEntity
			tileEntity.setY(static_cast<float>(player1Row*TILE_SIZE + mapY));

			tileEntity.setEdge(COLLISION_BOX);
			// if collision between player and tile
			if (players[i].collidesWith(tileEntity, collisionVector))
				players[i].toOldPosition();
		}
		// if current tile may be collided with
		if (player1Col >= 0 && player1Row < MAP_HEIGHT && tileMap[player1Row][player1Col] <= garreth::MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>(player1Col*TILE_SIZE + mapX));     // position tileEntity
			tileEntity.setY(static_cast<float>(player1Row*TILE_SIZE + mapY));
			tileEntity.setEdge(COLLISION_BOX);
			// if collision between player and tile
			if (players[i].collidesWith(tileEntity, collisionVector))
				players[i].toOldPosition();
		}
		// if tile right of player may be collided with
		if (player1Col < MAP_WIDTH - 1 && player1Row < MAP_HEIGHT && tileMap[player1Row][player1Col + 1] <= garreth::MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>((player1Col + 1)*TILE_SIZE + mapX));   // position tileEntity
			tileEntity.setY(static_cast<float>(player1Row*TILE_SIZE + mapY));
			tileEntity.setEdge(COLLISION_BOX);
			// if collision between player and tile
			if (players[i].collidesWith(tileEntity, collisionVector))
				players[i].toOldPosition();
		}
		// if tile bottom left of player may be collided with
		if (player1Col > 0 && player1Row < MAP_HEIGHT - 1 && tileMap[player1Row + 1][player1Col - 1] <= garreth::MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>((player1Col - 1)*TILE_SIZE + mapX));   // position tileEntity
			tileEntity.setY(static_cast<float>((player1Row + 1)*TILE_SIZE + mapY));
			tileEntity.setEdge(COLLISION_BOX);
			// if collision between player and tile
			if (players[i].collidesWith(tileEntity, collisionVector))
				players[i].toOldPosition();
		}
		// if tile below player may be collided with
		if (player1Row < MAP_HEIGHT - 1 && tileMap[player1Row + 1][player1Col] <= garreth::MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>(player1Col*TILE_SIZE + mapX));     // position tileEntity
			tileEntity.setY(static_cast<float>((player1Row + 1)*TILE_SIZE + mapY));
			tileEntity.setEdge(COLLISION_BOX);
			// if collision between player and tile
			if (players[i].collidesWith(tileEntity, collisionVector))
				players[i].toOldPosition();
		}
		// if tile bottom right of player may be collided with
		if (player1Col < MAP_WIDTH - 1 && player1Row < MAP_HEIGHT - 1 && tileMap[player1Row + 1][player1Col + 1] <= garreth::MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>((player1Col + 1)*TILE_SIZE + mapX));   // position tileEntity
			tileEntity.setY(static_cast<float>((player1Row + 1)*TILE_SIZE + mapY));
			tileEntity.setEdge(COLLISION_BOX);
			// if collision between player and tile
			if (players[i].collidesWith(tileEntity, collisionVector))
				players[i].toOldPosition();
		}
	}
}

//=============================================================================
// Render game items
//=============================================================================
void Bomberman::render()
{
	graphics->spriteBegin();
	//Draw the tile map
	for (int row = 0; row<MAP_HEIGHT; row++)       // for each row of map
	{
		tile.setY((float)(row*TILE_SIZE)); // set tile Y
		for (int col = 0; col<MAP_WIDTH; col++)    // for each column of map
		{
			if (tileMap[row][col] >= 0)          // if tile present
			{
				tile.setX((float)(col*(TILE_SIZE)) + mapX);  // set tile X
				tile.setY((float)(row*(TILE_SIZE)) + mapY);  // set tile Y
				// if tile on screen
				if ((tile.getX() > -TILE_SIZE && tile.getX() < GAME_WIDTH) &&
					(tile.getY() > -TILE_SIZE && tile.getY() < GAME_HEIGHT))
					tile.draw(UINT(tileMap[row][col]));             // draw tile
			}
		}
	}

	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		players[i].draw();
	}

	//draw the side images
	Player1Image.draw();
	Player2Image.draw();
	//draw the destructible blocks
	for (int i = 0; i < garreth::NUMBER_OF_DBLOCKS; i++){

			dBlocks[i].draw();
		
	}
	//draw the powerups
	for (int i = 0; i < garreth::NUMBER_OF_POWERUPS; i++){
		powerUps[i].draw();
	}
	
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		bombs[i].draw();
	}
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		explosionSmall[i].draw();
		explosionMid[i].draw();
		explosionLarge[i].draw();
	}
	//Text 
	fontScore.print("Player1: " + std::to_string(players[0].getScore()), 100, 10);
	dxFont.print("Player2: " + std::to_string(players[1].getScore()), 1300, 10);
	fontScoreP3.print("Player3: " + std::to_string(players[2].getScore()), 1300, 300);
	fontScoreP5.print("Player5: " + std::to_string(players[4].getScore()), 1300, 550);
	fontScoreP4.print("Player4: " + std::to_string(players[3].getScore()), 100, 350);
	 //  if (!roundOver){
	//	   roundTimeFont.print(std::to_string(min) + ":" + std::to_string((int)sec), 700, 10);
	//   }
	if (countDownOn)
	{	
		roundTimeFont.print(std::to_string((int)countDownTimer), GAME_WIDTH / 2, 300);
	}
	
	if (controlsOn)
	{
		controls.draw();
		menuSprite4.draw();
		//pointer.draw();
	}
	if (menuOn)
	{

		menu.draw();
		menuSprite.draw();
		menuSprite2.draw();
		menuSprite3.draw();
		pointer.draw();
		//audio->playCue(MENU);
		//toClientData.sounds |= MENU_BIT; // sound on
	}
	
		
	graphics->spriteEnd(); 
}


//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Bomberman::releaseAll()
{
	playerTextures.onLostDevice();
	menuTextures.onLostDevice();
	menuSpriteTexture.onLostDevice();
	menuSpriteTexture2.onLostDevice();
	menuSpriteTexture3.onLostDevice();
	menuSpriteTexture4.onLostDevice();
	pointerTextures.onLostDevice();
	controlTextures.onLostDevice();
	tileTextures.onLostDevice();
	dBlockTexture.onLostDevice();
	bombTexture.onLostDevice();
	powerUpTexture.onLostDevice();
	explosionTexture.onLostDevice();
	//SAFE_ON_LOST_DEVICE(fontScore);
	//SAFE_ON_LOST_DEVICE(dxFont);
	Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Bomberman::resetAll()
{
	//SAFE_ON_RESET_DEVICE(fontScore);
	//SAFE_ON_RESET_DEVICE(dxFont);
	playerTextures.onResetDevice();
	menuTextures.onResetDevice();
	menuSpriteTexture.onResetDevice();
	menuSpriteTexture2.onResetDevice();
	menuSpriteTexture3.onResetDevice();
	menuSpriteTexture4.onResetDevice();
	pointerTextures.onResetDevice();
	controlTextures.onResetDevice();
	tileTextures.onResetDevice();
	dBlockTexture.onResetDevice();
	powerUpTexture.onResetDevice();
	bombTexture.onResetDevice();
	explosionTexture.onResetDevice();
    Game::resetAll();
    return;
}
//=============================================================================
// Name: roundStart
// Description:Resets the game state and variables when the round starts
// Parameters: none
// Returns: none
// Garreth
//=============================================================================
void Bomberman::roundStart()
{
	
	//roundCount = 0;
	//sec = 0;
	//min = garreth::ROUND_TIME;
	
	countDownTimer = garreth::ROUND_COUNT_DOWN;
	nextRoundTimer = garreth::END_ROUND_COUNT_DOWN;
	loadLevel();
	toClientData.sounds |= MUSIC1_BIT; // sound on
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		players[i].resetStats();
	}
	players[0].setX(garreth::PLAYER1_SPAWNX);
	players[0].setY(garreth::PLAYER1_SPAWNY);
	players[1].setX(garreth::PLAYER2_SPAWNX);

	players[1].setY(garreth::PLAYER1_SPAWNY);
	players[2].setX(1016);
	players[2].setY(360);
	players[3].setX(garreth::PLAYER1_SPAWNX);
	players[3].setY(garreth::PLAYER2_SPAWNY);
	//	players[4].setX(garreth::PLAYER5_SPAWNX);
	//players[4].setY(garreth::PLAYER5_SPAWNY);
	players[4].setX(garreth::PLAYER2_SPAWNX);
	players[4].setY(garreth::PLAYER2_SPAWNY);

	countDownOn = true;
	roundOver = false;
	//console->hide();
	// set the state to indicate new round
	toClientData.gameState |= ROUND_START_BIT;
}
//=============================================================================
// Name: handleBombs
// Description: handles the planting of bombs when the player wants to plant a bomb
// Parameters: pointer a player
// Returns: none
// Garreth
//=============================================================================
void Bomberman::handleBombs(Player *player){
	if (player->getID() == 1){
		bombs[0].plant(player, player->getBombTimer());
	}
	else if (player->getID() == 2){
		bombs[1].plant(player, player->getBombTimer());
	}
	else if (player->getID() == 3){
		bombs[2].plant(player, player->getBombTimer());
	}
	else if (player->getID() == 4){
		bombs[3].plant(player, player->getBombTimer());
	}
	else if (player->getID() == 5){
		bombs[4].plant(player, player->getBombTimer());
	}
}


//=============================================================================
// Name: loadLevel
// Description: This function sets the postion of the destructible blocks on the map
// Parameters: none
// Returns: none
// Garreth
//=============================================================================
void Bomberman::loadLevel(){
	// load map
	
	int count = 0;
	int step = 0;
	int stepSize = 0;
	for (int i = 0; i < 7; i++){

		if (!dBlocks[i].initialize(this, 0, 0, 0, &dBlockTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}

		dBlocks[i].setID(i);
		dBlocks[i].setX(696 + count);
		dBlocks[i].setY(232 + stepSize);
		dBlocks[i].setEdge(garreth::TILE_EDGE);
		count = count + 32;
		dBlocks[i].setActive(true);
	}
	count = 0;
	for (int i = 7; i < 16; i++){
		if (!dBlocks[i].initialize(this, 0, 0, 0, &dBlockTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}
		dBlocks[i].setID(i);
		dBlocks[i].setX(664 + count);
		dBlocks[i].setY(264);
		dBlocks[i].setEdge(garreth::TILE_EDGE);
		count = count + 32;
		dBlocks[i].setActive(true);
	}
	count = 0;
	for (int i = 17; i < 72; i++){
		if (!dBlocks[i].initialize(this, 0, 0, 0, &dBlockTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}
		dBlocks[i].setID(i);
		dBlocks[i].setX(632 + count);
		dBlocks[i].setY(296 + stepSize);
		dBlocks[i].setEdge(garreth::TILE_EDGE);
		count = count + 32;
		if (i == 27 || i == 38 || i == 49 || i == 60 || i == 71){
			stepSize += 32;
			count = 0;
		}
		dBlocks[i].setActive(true);
	}
	count = 0;
	for (int i = 73; i < 82; i++){
		if (!dBlocks[i].initialize(this, 0, 0, 0, &dBlockTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}
		dBlocks[i].setID(i);
		dBlocks[i].setX(664 + count);
		dBlocks[i].setY(456);
		dBlocks[i].setEdge(garreth::TILE_EDGE);
		count = count + 32;
		dBlocks[i].setActive(true);
	}
	count = 0;
	for (int i = 83; i < 90; i++){

		if (!dBlocks[i].initialize(this, 0, 0, 0, &dBlockTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}

		dBlocks[i].setID(i);
		dBlocks[i].setX(696 + count);
		dBlocks[i].setY(488);
		dBlocks[i].setEdge(garreth::TILE_EDGE);
		count = count + 32;

		dBlocks[i].setActive(true);

	}
	dBlocks[33].setActive(false);
	dBlocks[43].setActive(false);
	dBlocks[44].setActive(false);
	dBlocks[45].setActive(false);
	dBlocks[55].setActive(false);
	dBlocks[33].setVisible(false);
	dBlocks[43].setVisible(false);
	dBlocks[44].setVisible(false);
	dBlocks[45].setVisible(false);
	dBlocks[55].setVisible(false);
	ResetLevel();
}
//=============================================================================
// Name: ResetLevel
// Description: Resets the blocks, powerups, bombs and explosions when the round ends
// Parameters: none
// Returns: none
// Garreth
//=============================================================================
void Bomberman::ResetLevel(){
	for (int i = 0; i < garreth::NUMBER_OF_DBLOCKS; i++){
		if (!dBlocks[i].getActive()){
			dBlocks[i].setActive(true);
			dBlocks[i].setVisible(true);
		}
	}
	for (int i = 0; i < garreth::NUMBER_OF_POWERUPS; i++){
		if (powerUps[i].getActive()){
			powerUps[i].setActive(false);
			powerUps[i].setVisible(false);
		}
	}
	for (int i = 0; i < garreth::MAX_PLAYERS; i++){
		if (bombs[i].getActive()){
			bombs[i].setActive(false);
			bombs[i].setVisible(false);
		}
	}
	
}
//=============================================================================
// process console commands
//=============================================================================
void Bomberman::consoleCommand()
{
	command = console->getCommand();    // get command from console
	if (command == "")                   // if no command
		return;

	if (command == "help")              // if "help" command
	{
		console->print(" ");
		console->print("Console Commands:");
		console->print("~ - show/hide console");
		console->print("fps - toggle display of frames per second");
		console->print("gravity off - turns off planet gravity");
		console->print("gravity on - turns on planet gravity");
		console->print("port # - sets port number, CAUTION! Restarts server");
		return;
	}
	else if (command == "fps")
	{
		fpsOn = !fpsOn;                 // toggle display of fps
		if (fpsOn)
			console->print("fps On");
		else
			console->print("fps Off");
	}
	else if (command == "gravity off")
	{
		console->print("Gravity Off");
	}
	else if (command == "gravity on")
	{
		console->print("Gravity On");
	}
	else if (command.substr(0, 4) == "port")
	{
		int newPort = atoi(command.substr(5).c_str());
		if (newPort > netNS::MIN_PORT && newPort < 65536)
		{
			port = newPort;             // set new port
			countDownOn = false;
			playerCount = 0;
			netTime = 0;
			initializeServer(port);     // re-initialize game server
			roundOver = true;
		}
		else
			console->print("Invalid port number");
	}
}
void Bomberman::pointerChange()
{
	if (state == START_OPTION)
	{
		pointer.setX(1100);
		pointer.setY(400);
	}
	if (state == OPTIONS_OPTION)
	{
		pointer.setX(1100);
		pointer.setY(500);
	}
	if (state == EXIT_OPTION)
	{
		pointer.setX(1100);
		pointer.setY(600);

	}
	if (state == BACK_OPTION)
	{
		pointer.setX(680);
		pointer.setY(400);
	}
}
//=============================================================================
// Name: handleTimer
// Description: Handles the round timer for the round
// Parameters: frametime
// Returns: none
// Garreth
//=============================================================================
void Bomberman::handleTimer(float frameTime){
	if ( !roundOver){
		sec -= frameTime;
		if (sec < 0){
			sec = 60;
			min--;
			roundCount++;
		}
	}
}

void Bomberman::handleMenuControls(float frameTime){
	SHORT movementX = input->getGamepadThumbLX(0);
	SHORT movementY = input->getGamepadThumbLY(0);

	if (menuOn || controlsOn)
	{
		if (input->wasKeyPressed(PLAYER2_UP_KEY) || input->getGamepadDPadUp(0) || movementY > 0)
		{
			if (state == START_OPTION){
				state = EXIT_OPTION;
				pointerChange();
			}
			else if (state == OPTIONS_OPTION){
				state = START_OPTION;
				pointerChange();
			}
			else if (state == EXIT_OPTION){
				state = OPTIONS_OPTION;
				pointerChange();
			}
		}
		else if (input->wasKeyPressed(PLAYER2_DOWN_KEY) || input->getGamepadDPadDown(0) || movementY < 0)
		{

			if (state == START_OPTION){
				state = OPTIONS_OPTION;
				pointerChange();
			}
			else if (state == OPTIONS_OPTION){
				state = EXIT_OPTION;
				pointerChange();
			}
			else if (state == EXIT_OPTION){
				state = START_OPTION;
				pointerChange();
			}
		}

		else if (input->isKeyDown(PLAYER1_DROP_KEY) || input->getGamepadA(0))
		{
			if (state == START_OPTION){
				if (menuOn)
				{
					controlsOn = false;
					menuOn = false;
					isPlaying = true;
					countDownOn = true;
					input->clearAll();
					roundStart();
				}
			}
			else	if (state == OPTIONS_OPTION){
				if (controlsOn)
				{

					menuOn = false;
					controlsOn = true;
					input->clearAll();
					state = BACK_OPTION;
					pointerChange();
				}
			}
			else if (state == EXIT_OPTION){
				PostQuitMessage(0);
			}

		}

		else if ( input->getGamepadB(0) && state == BACK_OPTION){
			menuOn = true;
			input->clearAll();
			state = START_OPTION;
			pointerChange();
		}
	}
}

////////////////////////////
//   Network Functions    //
////////////////////////////

//=============================================================================
// Initialize Server
//=============================================================================
int Bomberman::initializeServer(int port)
{
	std::stringstream ss;

	if (port < netNS::MIN_PORT)
	{
		console->print("Invalid port number");
		return netNS::NET_ERROR;
	}
	// ----- Initialize network stuff -----
	error = net.createServer(port, netNS::UDP);
	if (error != netNS::NET_OK)              // if error
	{
		console->print(net.getError(error));
		return netNS::NET_ERROR;
	}

	for (int i = 0; i< MAX_PLAYERS; i++)       // for all players
	{
		players[i].setActive(false);
		players[i].setConnected(false);
		players[i].setScore(0);
	}

	console->print("----- Server -----");
	net.getLocalIP(localIP);
	ss << "Server IP: " << localIP;
	console->print(ss.str());
	ss.str("");                             // clear stringstream
	ss << "Port: " << port;
	console->print(ss.str());
	return netNS::NET_OK;
}

//=============================================================================
// Do network communications
//=============================================================================
void Bomberman::communicate(float frameTime)
{
	// communicate with client
	// this function is not delayed so client response is as fast as possible
	doClientCommunication();

	// calculate elapsed time for network communications
	netTime += frameTime;
	if (netTime < netNS::NET_TIME)      // if not time to communicate
		return;
	netTime -= netNS::NET_TIME;

	// check for inactive clients, called every NET_TIME seconds
	checkNetworkTimeout();
}

//=============================================================================
// Check for network timeout
//=============================================================================
void Bomberman::checkNetworkTimeout()
{
	std::stringstream ss;

	for (int i = 0; i<MAX_PLAYERS; i++)       // for all players
	{
		if (players[i].getConnected())
		{
			players[i].incTimeout();               // timeout++
			// if communication timeout
			if (players[i].getTimeout() > netNS::MAX_ERRORS)
			{
				players[i].setConnected(false);
				ss << "***** Player " << i << " disconnected. *****";
				console->print(ss.str());
			}
		}
	}
}

//=============================================================================
// Do client communication
// Called by server to send game state to each client
//=============================================================================
void Bomberman::doClientCommunication()
{
	int playN;                  // player number we are communicating with
	int size;
	prepareDataForClient();     // prepare data for transmission to clients

	for (int i = 0; i<MAX_PLAYERS; i++)   // for all players
	{
		size = sizeof(toServerData);
		if (net.readData((char*)&toServerData, size, remoteIP) == netNS::NET_OK)
		{
			if (size > 0)                // if data received
			{
				playN = toServerData.playerN;
				if (playN == 255)       // if request to join game
				{
					clientWantsToJoin();
				}
				else if (playN >= 0 && playN < MAX_PLAYERS)  // if valid playerN
				{
					if (players[playN].getConnected()) // if this player is connected
					{
						if (players[playN].getActive()) // if this player is active
							players[playN].setButtons(toServerData.buttons);
						size = sizeof(toClientData);
						// send player the latest game data
						net.sendData((char*)&toClientData, size, players[playN].getNetIP());
						players[playN].setTimeout(0);
						players[playN].setCommWarnings(0);
					}
				}
			}
		}
		else    // no more incomming data
		{
			break;
		}
	}
}

//=============================================================================
// Prepare data to send to client. It contains information on all players.
//=============================================================================
void Bomberman::prepareDataForClient()
{
	for (int i = 0; i < MAX_PLAYERS; i++)       // for all players
	{
		toClientData.playerClient[i].playerData = players[i].getNetData();
		toClientData.playerClient[i].bombData = bombs[i].getNetData();
		
		toClientData.playerClient[i].explosionData = explosionSmall[i].getNetData();
		toClientData.playerClient[i].explosionData = explosionMid[i].getNetData();
		toClientData.playerClient[i].explosionData = explosionLarge[i].getNetData();

	
		
	}
	for (int j = 0; j < garreth::NUMBER_OF_POWERUPS; j++){
		toClientData.powerClient[j].powerUpData = powerUps[j].getNetData();
	}
	for (int i = 0; i < garreth::NUMBER_OF_DBLOCKS; i++){	
		toClientData.blockClient[i].dBlockData = dBlocks[i].getNetData();
		

	//for (int j = 0; j < garreth::NUMBER_OF_DBLOCKS; j++){
		//toClientData.playerClient[i].dBlockData = dBlocks[i].getNetData();
	//	}
//	for (int j = 0; j < garreth::NUMBER_OF_POWERUPS; j++){
	//	toClientData.playerClient[i].powerUpData = powerUps[i].getNetData();
	//}

	}
		
}

//=============================================================================
// Client is requesting to join game
//=============================================================================
void Bomberman::clientWantsToJoin()
{
	std::stringstream ss;
	int size;
	int status;

	connectResponse.number = 255;       // set to invalid player number

	if (playerCount == 0)                // if no players currently in game
	{
		roundOver = true;               // start a new round
		for (int i = 0; i<MAX_PLAYERS; i++)    // for all players
			players[i].setScore(0);        // reset score
	}

	console->print("Player requesting to join.");
	// find available player position to use
	for (int i = 0; i<MAX_PLAYERS; i++)        // search all player positions
	{
		if (players[i].getConnected() == false)    // if this position available
		{
			players[i].setConnected(true);
			players[i].setTimeout(0);
			players[i].setCommWarnings(0);
			players[i].setNetIP(remoteIP);     // save player's IP
			players[i].setCommErrors(0);       // clear old errors
			// send SERVER_ID and player number to client
			strcpy_s(connectResponse.response, netNS::SERVER_ID);
			connectResponse.number = (UCHAR)i;
			size = sizeof(connectResponse);
			status = net.sendData((char*)&connectResponse, size, remoteIP);
			if (status == netNS::NET_ERROR)
			{
				console->print(net.getError(status));   // display error message
				return;
			}
			toServerData.playerN = i;       // clear join request from input buffer                
			ss << "Connected player as number: " << i;
			console->print(ss.str());
			return;                         // found available player position
		}
	}
	// send SERVER_FULL to client
	strcpy_s(connectResponse.response, netNS::SERVER_FULL);
	size = sizeof(connectResponse);
	status = net.sendData((char*)&connectResponse, size, remoteIP);
	console->print("Server full.");
}

